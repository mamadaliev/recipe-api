package com.example.recipe.transfers;

import lombok.Data;

@Data
public class IngredientTransfer {

    private long id;
    private String name;
}
