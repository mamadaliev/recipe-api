package com.example.recipe.services.impl;

import com.example.recipe.models.Ingredient;
import com.example.recipe.repositories.IngredientRepository;
import com.example.recipe.repositories.RecipeRepository;
import com.example.recipe.services.IngredientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@Transactional
public class IngredientServiceImpl implements IngredientService {

    private final RecipeRepository recipeRepository;
    private final IngredientRepository ingredientRepository;

    @Autowired
    public IngredientServiceImpl(RecipeRepository recipeRepository, IngredientRepository ingredientRepository) {
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
    }

    @Override
    public Ingredient add(Ingredient ingredient) {
        if (!recipeRepository.existsByName(ingredient.getRecipe().getName())) {
            ingredient.setRecipe(recipeRepository.findByName(ingredient.getRecipe().getName()));
            return ingredientRepository.save(ingredient);
        } else {
            throw new RuntimeException("Ingredient Already Exists");
        }
    }

    @Override
    public List<Ingredient> getAll() {
        return ingredientRepository.findAll();
    }

    @Override
    public Ingredient getById(long id) {
        return ingredientRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Ingredient Not Found"));
    }

    @Override
    public List<Ingredient> getAllByRecipeId(long id) {
        return ingredientRepository.findAllByRecipe_Id(id);
    }
}
