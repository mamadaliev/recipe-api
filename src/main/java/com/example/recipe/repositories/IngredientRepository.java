package com.example.recipe.repositories;

import com.example.recipe.models.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Long> {

    boolean existsByName(String name);
    List<Ingredient> findAllByRecipe_Id(long id);
}
