package com.example.recipe.repositories;

import com.example.recipe.models.Ingredient;
import com.example.recipe.models.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {

    boolean existsByName(String name);

    Recipe findByName(String name);
}
