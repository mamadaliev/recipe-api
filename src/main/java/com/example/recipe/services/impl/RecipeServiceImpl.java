package com.example.recipe.services.impl;

import com.example.recipe.models.Recipe;
import com.example.recipe.repositories.RecipeRepository;
import com.example.recipe.services.RecipeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@Transactional
public class RecipeServiceImpl implements RecipeService {

    private final RecipeRepository recipeRepository;

    @Autowired
    public RecipeServiceImpl(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    @Override
    public Recipe add(Recipe recipe) {
        if (recipe == null) throw new RuntimeException("Recipe is null");
        return recipeRepository.save(recipe);
    }

    @Override
    public List<Recipe> getAll() {
        return recipeRepository.findAll();
    }

    @Override
    public Recipe getById(long id) {
        return recipeRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Recipe Not found"));
    }
}
