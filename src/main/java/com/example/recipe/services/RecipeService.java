package com.example.recipe.services;

import com.example.recipe.models.Recipe;

import java.util.List;

public interface RecipeService {

    Recipe add(Recipe recipe);

    List<Recipe> getAll();

    Recipe getById(long id);
}
