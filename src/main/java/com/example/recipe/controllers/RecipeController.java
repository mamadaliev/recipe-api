package com.example.recipe.controllers;

import com.example.recipe.models.Ingredient;
import com.example.recipe.models.Recipe;
import com.example.recipe.repositories.IngredientRepository;
import com.example.recipe.services.IngredientService;
import com.example.recipe.services.RecipeService;
import com.example.recipe.transfers.RecipeTransfer;
import lombok.extern.slf4j.Slf4j;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/recipes")
public class RecipeController {

    private final RecipeService recipeService;
    private final IngredientService ingredientService;
    private final Mapper mapper;

    @Autowired
    public RecipeController(RecipeService recipeService, IngredientService ingredientService, Mapper mapper, IngredientRepository ingredientRepository) {
        this.recipeService = recipeService;
        this.ingredientService = ingredientService;
        this.mapper = mapper;
    }

    @PostMapping("/add")
    public RecipeTransfer add(@RequestBody RecipeTransfer recipe) {
        Recipe r = mapper.map(recipe, Recipe.class);
        for (Ingredient i : r.getIngredients()) {
            i.setRecipe(r);
        }
        return mapper.map(recipeService.add(r), RecipeTransfer.class);
    }

    @GetMapping("/all")
    public List<RecipeTransfer> getAll() {
        List<Recipe> recipes = recipeService.getAll();
        for (Recipe recipe : recipes) {
            recipe.setIngredients(ingredientService.getAllByRecipeId(recipe.getId()));
        }

        return recipeService.getAll().stream()
                .map(e -> mapper.map(e, RecipeTransfer.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public RecipeTransfer getById(@PathVariable long id) {
        Recipe recipe = recipeService.getById(id);
        recipe.setIngredients(ingredientService.getAllByRecipeId(recipe.getId()));
        return mapper.map(recipe, RecipeTransfer.class);

    }
}
